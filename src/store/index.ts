import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentItem: null
  },
  mutations: {
    SET_CURRENT_ITEM(state, payload) {
      state.currentItem = payload;
    }
  },
  actions: {
    setCurrentItem({ state, commit }, payload) {
      commit('SET_CURRENT_ITEM', payload);
    }
  },
  modules: {
  }
})
